import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditEnterpriseComponent } from './components/edit-enterprise/edit-enterprise.component';
import { GameComponent } from './components/game/game.component';
import { EnterprisesComponent } from './components/enterprises/enterprises.component';
import { EditGameComponent } from './components/edit-game/edit-game.component';
import { AddEnterpriseComponent } from './components/add-enterprise/add-enterprise.component';
import { AddGameComponent } from './components/add-game/add-game.component';
import { GotyComponent } from './components/goty/goty.component';


const routes: Routes = [
  { path: '', component: EnterprisesComponent },
  { path: 'enterprise/:enterpriseId', component: EditEnterpriseComponent },
  { path: 'enterprise/:enterpriseId/games', component: GameComponent },
  { path: 'enterprise/:enterpriseId/games/:gameId', component: EditGameComponent },
  //Para el HTML
  { path: 'addEnterprise', component: AddEnterpriseComponent },
  //New
  { path: 'GOTY', component: GotyComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
