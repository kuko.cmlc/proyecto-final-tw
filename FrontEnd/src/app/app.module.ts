import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnterprisesComponent } from './components/enterprises/enterprises.component';
import { EnterpriseItemComponent } from './components/enterprise-item/enterprise-item.component';
import { AddEnterpriseComponent } from './components/add-enterprise/add-enterprise.component';
import { EditEnterpriseComponent } from './components/edit-enterprise/edit-enterprise.component';
import { GameComponent } from './components/game/game.component';
import { GameItemComponent } from './components/game-item/game-item.component';
import { AddGameComponent } from './components/add-game/add-game.component';
import { EditGameComponent } from './components/edit-game/edit-game.component';
import { HeaderComponent } from './components/header/header.component';
import { GotyComponent } from './components/goty/goty.component';
import { GotyItemComponent } from './components/goty-item/goty-item.component';

@NgModule({
  declarations: [
    AppComponent,
    EnterprisesComponent,
    EnterpriseItemComponent,
    AddEnterpriseComponent,
    EditEnterpriseComponent,
    GameComponent,
    GameItemComponent,
    AddGameComponent,
    EditGameComponent,
    HeaderComponent,
    GotyComponent,
    GotyItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,    
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
