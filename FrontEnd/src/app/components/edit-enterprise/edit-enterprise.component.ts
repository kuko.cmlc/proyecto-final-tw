import { Component, OnInit } from '@angular/core';
import { Enterprise } from 'src/app/models/Enterprise';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-enterprise',
  templateUrl: './edit-enterprise.component.html',
  styleUrls: ['./edit-enterprise.component.css']
})
export class EditEnterpriseComponent implements OnInit {
  enterprise:Enterprise;

  constructor(private enterpriseService:EnterpriseService, private route: ActivatedRoute, private router: Router) {
    this.enterprise = new Enterprise();
   }

  ngOnInit() {
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    this.enterpriseService.getEnterprise(enterpriseId).subscribe(e => {
      this.enterprise = e;
    });      
   
  }

  onSubmit(enterprise:Enterprise){
    this.enterpriseService.editEnterprise(enterprise).subscribe(e => {
      console.log(e); 
    });
    this.router.navigateByUrl(``);    
    
  }

}
