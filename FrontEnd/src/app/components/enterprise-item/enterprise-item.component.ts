import { Component, OnInit, Input,EventEmitter,Output} from '@angular/core';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { Enterprise } from 'src/app/models/Enterprise';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enterprise-item',
  templateUrl: './enterprise-item.component.html',
  styleUrls: ['./enterprise-item.component.css']
})
export class EnterpriseItemComponent implements OnInit {
  @Input() enterprise: Enterprise;
  @Output() deleteEnterprise: EventEmitter<Enterprise> = new EventEmitter();


  constructor(private EnterpriseService:EnterpriseService, private router:  Router) { }

  ngOnInit() {
  }
  
  onDelete(enterprise){
    this.deleteEnterprise.emit(enterprise);
  }
  
  onEdit(enterprise:Enterprise) {
    this.router.navigateByUrl(`/enterprise/${enterprise.id}`);
  }

  onGetGames(enterprise){
    this.router.navigateByUrl(`/enterprise/${enterprise.id}/games`);
  }
}
