import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { Game } from 'src/app/models/Game';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.css']
})
export class GameItemComponent implements OnInit {
  @Input() game: Game;
  @Output() deleteGame: EventEmitter<Game> = new EventEmitter();
  
  sales:number;

  constructor(private enterpriseService:EnterpriseService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit() {
  }
  onDeleteGame(game){
    this.deleteGame.emit(game);
  }
  onEditGame(game:Game) {
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    this.router.navigateByUrl(`/enterprise/${enterpriseId}/games/${game.id}`);
  }
  
  OnEditSales(game:Game){
   const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
   game.sales = this.sales
   console.log(this.sales)
   this.enterpriseService.editSales(this.game, enterpriseId).subscribe(g => {
  });
 }
}
