import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent implements OnInit {
  @Output() addGame: EventEmitter<any> = new EventEmitter();

  name:string;
  genre:string;
  yearLauch:number;
  rangeAge:string;
  platform:string;
  url_image: string;
  sales: number;

  constructor() { }

  ngOnInit() {
  }
  onSubmit(){
    const game={
      name:this.name,
      genre:this.genre,
      yearLauch:this.yearLauch,
      rangeAge:this.rangeAge,
      platform:this.platform,
      url_image:this.url_image,
      sales:this.sales
    }
    this.addGame.emit(game);
    
  }
}
