import { Component,OnInit} from '@angular/core';
import { Enterprise } from 'src/app/models/Enterprise';
import { EnterpriseService } from 'src/app/services/enterprise.service';

@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.css']
})
export class EnterprisesComponent implements OnInit {
  
  
  enterprises: Enterprise[];
  enterprise: Enterprise;
  word: string

  constructor(private EnterpriseService:EnterpriseService) { }

  ngOnInit() {
    this.EnterpriseService.refreshNeed$.subscribe(() => {
      this.GetEnterprises();
    });
    this.GetEnterprises();
    }

  private GetEnterprises (){  
    this.EnterpriseService.getEnterprises().subscribe(enterprises => {
      this.enterprises = enterprises;
    })
  }
  deleteEnterprise(enterprise:Enterprise){
    this.enterprises = this.enterprises.filter(e => e.id !== enterprise.id);
    this.EnterpriseService.deleteEnterprise(enterprise).subscribe();
  }
  
  OnSearch(){
    console.log(this.word)
    if(this.word == '')
    {
      this.GetEnterprises();
    }
    else
    {
      this.enterprises = this.enterprises.filter(e => e.name.includes(this.word));
      if (this.enterprises.length > 0) 
      {
        this.enterprises = this.enterprises.filter(e => e.name.includes(this.word));
      }
      else
      {
        this.GetEnterprises();
      }
    }
   }

}
