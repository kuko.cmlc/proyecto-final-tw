import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { Game } from 'src/app/models/Game';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-goty-item',
  templateUrl: './goty-item.component.html',
  styleUrls: ['./goty-item.component.css']
})
export class GotyItemComponent implements OnInit {
  @Input() game: Game;
  @Output() deleteGame: EventEmitter<Game> = new EventEmitter();
  constructor(private EnterpriseService:EnterpriseService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit() {
  }

}
