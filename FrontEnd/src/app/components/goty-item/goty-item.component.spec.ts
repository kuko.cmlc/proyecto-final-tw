import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GotyItemComponent } from './goty-item.component';

describe('GotyItemComponent', () => {
  let component: GotyItemComponent;
  let fixture: ComponentFixture<GotyItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GotyItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GotyItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
