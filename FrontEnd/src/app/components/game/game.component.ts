import { Component, OnInit ,Input} from '@angular/core';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { Game } from 'src/app/models/Game';
import { ActivatedRoute } from '@angular/router';
import { Enterprise } from 'src/app/models/Enterprise';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  @Input() enterprise: Enterprise;
  games: Game[];
  game: Game;
  

  word:string;
  constructor(private enterpriseService:EnterpriseService,private route: ActivatedRoute) { }


  
  ngOnInit() {
    this.enterpriseService.refreshNeed$.subscribe(()=>{
      this.GetAllGames();
    });
    this.GetAllGames();
  }

  private GetAllGames(){
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    this.enterpriseService.getGames(enterpriseId).subscribe(games => {
      this.games = games;
    })
  }

  deleteGame(game:Game){
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    
    this.games = this.games.filter(g => g.id !== game.id);
    this.enterpriseService.deleteGame(game,enterpriseId).subscribe();
  }
  addGame(game:Game) {
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
   
    this.enterpriseService.addGame(game,enterpriseId).subscribe(game => {
      this.games.push(game);
    });
  }

  OnSearch(){
    console.log(this.word)
    if(this.word == '')
    {
      this.GetAllGames();
    }
    else
    {
      this.games = this.games.filter(e => e.name.includes(this.word));
      if (this.games.length > 0) 
      {
        this.games = this.games.filter(e => e.name.includes(this.word));
      }
      else
      {
        this.GetAllGames();
      }
    }
  }
}
