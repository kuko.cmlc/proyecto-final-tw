import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/models/Game';
import { EnterpriseService } from 'src/app/services/enterprise.service';

@Component({
  selector: 'app-goty',
  templateUrl: './goty.component.html',
  styleUrls: ['./goty.component.css']
})
export class GotyComponent implements OnInit {

  games: Game[];

  constructor(private EnterpriseService:EnterpriseService) { }

  ngOnInit() {
    this.EnterpriseService.refreshNeed$.subscribe(() => {
      this.Get_GOTY();
    })
    this.Get_GOTY();
  }
  private Get_GOTY(){
    this.EnterpriseService.getGoty().subscribe(games => {
      this.games = games;
    })
  }
}
