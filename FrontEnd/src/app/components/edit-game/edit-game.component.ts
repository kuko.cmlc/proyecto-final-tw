import { Component, OnInit } from '@angular/core';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from 'src/app/models/Game';

@Component({
  selector: 'app-edit-game',
  templateUrl: './edit-game.component.html',
  styleUrls: ['./edit-game.component.css']
})
export class EditGameComponent implements OnInit {
  game:Game;
  constructor(private enterpriseService:EnterpriseService, private route: ActivatedRoute, private router: Router) {
    this.game = new Game();}

  ngOnInit() {
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    const gameId = this.route.snapshot.paramMap.get("gameId");
    this.enterpriseService.getGame(gameId,enterpriseId).subscribe(g => {
      this.game = g;
    });  
  }
  onSubmit(game:Game){
    const enterpriseId = this.route.snapshot.paramMap.get("enterpriseId");
    this.enterpriseService.editGame(game, enterpriseId).subscribe(g => {
      console.log(g); 
    });
    this.router.navigateByUrl(`enterprise/${enterpriseId}/games`);   
  }
}
