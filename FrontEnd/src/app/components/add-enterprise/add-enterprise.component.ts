import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { EnterpriseService } from 'src/app/services/enterprise.service';
import { Enterprise } from 'src/app/models/Enterprise';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-enterprise',
  templateUrl: './add-enterprise.component.html',
  styleUrls: ['./add-enterprise.component.css']
})
export class AddEnterpriseComponent implements OnInit {
 
  @Output() addEnterprise: EventEmitter<any> = new EventEmitter();
  
  name: string;
  postal_Code: number;
  country: string;
  address: string;
  city: string;
  number_Phone: number;
  mail: string;
  web_Page: string;
  url_image:string;
  enterprises: Enterprise[];

  constructor(private EnterpriseService:EnterpriseService,  private router: Router ) { }

  ngOnInit() {
    
  }
  onSubmit(){
    const enterprise={
      name:this.name,
      postal_Code:this.postal_Code,
      country:this.country,
      address:this.address,
      city:this.city,
      number_Phone:this.number_Phone,
      mail:this.mail,
      web_Page:this.web_Page,
      url_image:this.url_image
    }

    this.EnterpriseService.addEnterprise(enterprise).subscribe(enterprise => {
      this.enterprises.push(enterprise);
    });   
    this.router.navigateByUrl(``);    
  }
}
