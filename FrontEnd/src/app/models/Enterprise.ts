import { Game } from './Game';

export class Enterprise{
    id?: number;
    name: string;
    postal_Code: number;
    country: string;
    address: string;
    city: string;
    number_Phone: number;
    mail: string;
    web_Page: string;
    url_image: string;
}