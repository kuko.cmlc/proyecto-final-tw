import { Injectable } from '@angular/core';
import { Observable, Observer, BehaviorSubject, Subject } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Enterprise } from '../models/Enterprise';
import { Game } from '../models/Game';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {
  

  enterpriseUrl:string = 'https://localhost:44300/api/enterprise';

  constructor(private http:HttpClient) {}

  //Get Refresh Data
  private _refreshNeeded$ = new Subject<void>();
  get refreshNeed$(){
    return this._refreshNeeded$;
  }

  //Get Enterprises
  getEnterprises():Observable<Enterprise[]>{
    return this.http.get<Enterprise[]>(`${this.enterpriseUrl}`);   
  }
  //Get Enterprise
  getEnterprise(id:string):Observable<Enterprise>{
    return this.http.get<Enterprise>(`${this.enterpriseUrl}/${id}`);
  }
  //Delete Enterprise
  deleteEnterprise(enterprise:Enterprise):Observable<Enterprise>{
    const url = `${this.enterpriseUrl}/${enterprise.id}`;
    return this.http.delete<Enterprise>(url,httpOptions);
  }
  //Add Enterprise
  addEnterprise(enterprise:Enterprise):Observable<Enterprise>{
    return this.http.post<Enterprise>(this.enterpriseUrl, enterprise, httpOptions).pipe(tap(()=> {
      this._refreshNeeded$.next()
    }));;
  }
  //Edit Enterprise
  editEnterprise(enterprise:Enterprise):Observable<any>{
    const url = `${this.enterpriseUrl}/${enterprise.id}`;
    return this.http.put(url, enterprise, httpOptions).pipe(tap(()=> {
      this._refreshNeeded$.next()
    }));
  }




  //FOR GAMES

  //Get Games
  getGames(id:string):Observable<Game[]>{
    return this.http.get<Game[]>(`${this.enterpriseUrl}/${id}/games`);
  }

  //Get Enterprise
  getGame(idGame:string, idEnterprise:string):Observable<Game>{
    const url = `${this.enterpriseUrl}/${idEnterprise}`;
    return this.http.get<Game>(`${url}/games/${idGame}`);
  }

  //Delete Game
  deleteGame(game:Game,id:string):Observable<Game>{
    const url = `${this.enterpriseUrl}/${id}`;
    return this.http.delete<Game>(`${url}/games/${game.id}`,httpOptions);
  }
  //Add Game
  addGame(game:Game,id:string):Observable<Game>{
    const url = `${this.enterpriseUrl}/${id}`;
    return this.http.post<Game>(`${url}/games`, game, httpOptions).pipe(tap(()=> {
      this._refreshNeeded$.next()
    }));;
  }
  

  editGame(game:Game,id:string):Observable<any>{
    const url = `${this.enterpriseUrl}/${id}`;
    return this.http.put(`${url}/games/${game.id}`, game, httpOptions).pipe(tap(()=> {
      this._refreshNeeded$.next()
    }));
  }


  //NEW GOTY
  getGoty():Observable<Game[]>{
    return this.http.get<Game[]>(`${this.enterpriseUrl}/GOTY`)
  }

  //EDIT SALES
  editSales(game:Game,id:string):Observable<any>{
    const url = `${this.enterpriseUrl}/${id}`;
    return this.http.put(`${url}/games/${game.id}/updateSales`, game, httpOptions).pipe(tap(()=> {
      this._refreshNeeded$.next()
    }));
  }

}
