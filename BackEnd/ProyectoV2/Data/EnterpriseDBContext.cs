﻿using Microsoft.EntityFrameworkCore;
using ProyectoTecWeb.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Data
{
    public class EnterpriseDBContext : DbContext
    {
        public EnterpriseDBContext(DbContextOptions<EnterpriseDBContext> options)
            :base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EnterpriseEntity>().ToTable("Enterpises");
            modelBuilder.Entity<EnterpriseEntity>().HasMany(a => a.Games).WithOne(b =>b.Enterprise);
            modelBuilder.Entity<EnterpriseEntity>().Property(a => a.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<GameEntity>().ToTable("Games");
            modelBuilder.Entity<GameEntity>().Property(b => b.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<GameEntity>().HasOne(b => b.Enterprise).WithMany(a => a.Games);

        }
        public DbSet<EnterpriseEntity> Enterpises { get; set; }
        public DbSet<GameEntity> Games { get; set; }
      
    }
}
