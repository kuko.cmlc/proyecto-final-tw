﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Models;

namespace ProyectoTecWeb.Data.Repository
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        private EnterpriseDBContext enterpriseDBContext;
        public EnterpriseRepository(EnterpriseDBContext enterpriseDBContext)
        {
            this.enterpriseDBContext = enterpriseDBContext;
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await enterpriseDBContext.SaveChangesAsync()) > 0;
        }
        public void CreateEnterprise(EnterpriseEntity enterprise)
        {
            enterpriseDBContext.Enterpises.Add(enterprise);
        }
        public void CreateGame(GameEntity game)
        {
            enterpriseDBContext.Entry(game.Enterprise).State = EntityState.Unchanged;
            enterpriseDBContext.Games.Add(game);
        }
        public async Task DeleteEnterpriseAsync(int id)
        {
            var enterprise = await enterpriseDBContext.Enterpises.SingleAsync(a => a.Id == id);
            enterpriseDBContext.Enterpises.Remove(enterprise);
        }

        public async Task<IEnumerable<EnterpriseEntity>> GetEnterpisesAsync(bool showBooks = false, string orderBy = "id")
        {
            IQueryable<EnterpriseEntity> query = enterpriseDBContext.Enterpises;
            if (showBooks)
            {
                query = query.Include(a => a.Games);
            }
            switch (orderBy)
            {
                case "name":
                    query = query.OrderBy(a => a.Name);
                    break;
                case "country":
                    query = query.OrderBy(a => a.Country);
                    break;
                case "city":
                    query = query.OrderBy(a => a.City);
                    break;
                default:
                    query = query.OrderBy(a => a.Id);
                    break;
            }

            query = query.AsNoTracking();
            return  await query.ToArrayAsync();
        }

        public async Task<EnterpriseEntity> GetEnterpriseAsync(int id, bool showgames)
        {
            IQueryable<EnterpriseEntity> query = enterpriseDBContext.Enterpises;

            if (showgames)
            {
                query = query.Include(a => a.Games);
            }
            query = query.AsNoTracking();
            return await query.SingleOrDefaultAsync(a => a.Id == id);
        }

        public Task<GameEntity> GetGameAsync(int gameId, int EnterpriseId)
        {
            IQueryable<GameEntity> query = enterpriseDBContext.Games;
            query = query.AsNoTracking();
            return query.SingleAsync(b => b.Id == gameId && b.Enterprise.Id == EnterpriseId);
        }


        public void UpdateEnterprise(EnterpriseEntity enterprise)
        {
            enterpriseDBContext.Enterpises.Update(enterprise);
        }

        public void UpdateGame(GameEntity game)
        {
            enterpriseDBContext.Entry(game.Enterprise).State = EntityState.Unchanged;
            enterpriseDBContext.Games.Update(game);
        }

        public void DetachEntity<T>(T entity) where T : class
        {
            enterpriseDBContext.Entry(entity).State = EntityState.Detached;
        }

        public async Task<IEnumerable<GameEntity>> GetGamesAsync(int EnterpriseId)
        {
            IQueryable<GameEntity> query = enterpriseDBContext.Games;
            query = query.AsNoTracking();
            return await query.Where(b => b.Enterprise.Id == EnterpriseId).ToArrayAsync();
        }

        public async Task DeleteGameAsync(int id)
        {
            var game = await enterpriseDBContext.Games.SingleAsync(a => a.Id == id);
            enterpriseDBContext.Games.Remove(game);
        }




        //NEW
        public async Task<IEnumerable<GameEntity>> GetGoty6Async()
        {
            IQueryable<GameEntity> query = enterpriseDBContext.Games.OrderByDescending(s => s.Sales).Take(6);
            query = query.AsNoTracking();
            return await query.ToArrayAsync();


        }



    }
}
