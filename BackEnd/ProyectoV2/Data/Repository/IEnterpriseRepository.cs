﻿using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Data.Repository
{
    public interface IEnterpriseRepository
    {
        //Enterprise
        Task<EnterpriseEntity> GetEnterpriseAsync(int id, bool showgames = false);
        void CreateEnterprise(EnterpriseEntity enterprise);
        Task<IEnumerable<EnterpriseEntity>> GetEnterpisesAsync(bool showBooks = false, string orderBy = "id");
        Task DeleteEnterpriseAsync(int id);
        void UpdateEnterprise(EnterpriseEntity enterprise);


        //Game
        void CreateGame(GameEntity game);
        Task<GameEntity> GetGameAsync(int id,int idEnterprise);
        Task <IEnumerable<GameEntity>> GetGamesAsync(int idEnterprise);
        Task DeleteGameAsync(int id);
        void UpdateGame(GameEntity game);

        //General

        Task<bool> SaveChangesAsync();
        void DetachEntity<T>(T entity) where T : class;


        //New
        Task<IEnumerable<GameEntity>> GetGoty6Async();


    }
}
