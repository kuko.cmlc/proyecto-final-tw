﻿using AutoMapper;
using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Data
{
    public class EnterpriseProfile : Profile
    {
        public EnterpriseProfile()
        {
            this.CreateMap<EnterpriseEntity, Enterprise>()
                .ReverseMap();
            this.CreateMap<GameEntity, Game>()
                .ReverseMap();
        }
    }
}
