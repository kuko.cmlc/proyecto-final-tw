﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Data.Entities
{
    public class EnterpriseEntity
    {
        [Key, Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Postal_Code { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public int Number_Phone { get; set; }
        [Required]
        public string Mail { get; set; }
        public string Web_Page { get; set; }

        //New
        public string Url_Image { get; set; }

        public virtual ICollection<GameEntity> Games { get; set; }


    }
}
