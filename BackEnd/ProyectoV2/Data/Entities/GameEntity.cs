﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Data.Entities
{
    public class GameEntity
    {
        [Key, Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Genre { get; set; }
        [Required]
        public int YearLauch { get; set; }
        public string RangeAge { get; set; }
        public string Platform { get; set; }
        //new 
        public string Url_Image { get; set; }
        public int Sales { get; set; }

        [ForeignKey(nameof(EnterpriseEntity))]
        public virtual EnterpriseEntity Enterprise { get; set; }


    }
}
