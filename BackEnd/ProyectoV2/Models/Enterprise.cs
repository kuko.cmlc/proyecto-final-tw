﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Models
{
    public class Enterprise
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Postal_Code { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int Number_Phone { get; set; }
        public string Mail { get; set; }
        public string Web_Page { get; set; }

        //NEW
        public string Url_Image { get; set; }

        public IEnumerable<Game> games { get; set; }

  

    }
}
