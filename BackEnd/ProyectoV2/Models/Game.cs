﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Models
{
    public class Game
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public int YearLauch { get; set; }
        public string RangeAge { get; set; }
        public string Platform { get; set; }

        //NEW 
        public string Url_Image { get; set; }
        public int Sales { get; set; }


        [JsonIgnore]
        public int IdEnterprise { get; set; }

    }
}
