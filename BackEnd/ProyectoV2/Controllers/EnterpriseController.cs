﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectoTecWeb.Models;
using ProyectoTecWeb.Services;
using ProyectoV2.Exceptions;

namespace ProyectoV2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private IEnterpriseService enterpriseService;
        private IGameService gameService;

        public EnterpriseController(IEnterpriseService enterpriseService, IGameService gameService)
        {
            this.enterpriseService = enterpriseService;
            this.gameService = gameService;
        }
        //NEW 
        [HttpGet("{GOTY}")]
        public async Task<ActionResult<IEnumerable<Game>>> GetAllGames()
        {
            try
            {
                return Ok(await gameService.GetGoty());
            }
            catch (BadRequestOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");

            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetEnterprises(bool showgames = false, string orderBy = "id")
        {
            try
            {
                return Ok(await enterpriseService.GetEnterpisesAsync(showgames, orderBy));
            }
            catch (BadRequestOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");

            }
        }
        [HttpGet("{Enterpriseid:int}")]
        public async Task<ActionResult> GetEnterprise(int Enterpriseid, bool showgames = false)
        {
            try
            {
                var enterpriseDB = await this.enterpriseService.GetEnterpriseAsync(Enterpriseid, showgames);
                return Ok(enterpriseDB);
            }
            catch (NotFoundItemException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex )
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }
        [HttpPost]
        public async Task<ActionResult>CreateEnterprise(Enterprise enterprise)
        {
            if (ModelState.IsValid)
            {
                var postEnterprise = await this.enterpriseService.CreateEnterpriseAsync(enterprise);
                return Created($"/api/Enterprise/{postEnterprise.Id}", postEnterprise);
            }
            return BadRequest(ModelState);

        }
        [HttpDelete("{Enterpriseid:int}")]
        public async Task<ActionResult> DeleteEnterprise(int Enterpriseid)
        {
            try
            {
                if (await enterpriseService.GetEnterpriseAsync(Enterpriseid, false) != null)
                {
                    var games = await gameService.GetGames(Enterpriseid);
                    if (games != null)
                    {
                        foreach (var game in games)
                        {
                            int id = game.Id ?? default(int);
                            await gameService.DeleteGame(Enterpriseid,id);
                        }
                    }
                    bool resp = await enterpriseService.DeleteEnterpriseAsync(Enterpriseid);
                    if (resp != false)
                    {
                        return Ok();
                    }
                }
                return NotFound("Enterprise Not Found");
            }
            catch (NotFoundItemException ex)
            {

                return NotFound(ex.Message);
            }    
            catch   (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPut("{Enterpriseid:int}")]
        public async Task <ActionResult> UpdateEnterprise(int Enterpriseid, Enterprise enterprise)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var resp = await enterpriseService.UpdateEnterpriseAsync(Enterpriseid, enterprise);
                    if (resp != null)
                    {
                        return Ok();
                    }
                }
                catch (NotFoundItemException ex)
                {

                    return NotFound (ex.Message);
                }
                catch ( Exception ex)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
                }
            }
            return BadRequest("Could not update Enterprise");
        }
       
    }
}