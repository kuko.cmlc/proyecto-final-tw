﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectoTecWeb.Models;
using ProyectoTecWeb.Services;
using ProyectoV2.Exceptions;

namespace ProyectoV2.Controllers
{
    [Route("api/Enterprise/{EnterpriseId:int}/Games")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly IGameService gameService;

        public GamesController(IGameService gameService)
        {
            this.gameService = gameService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Game>>> getGames(int EnterpriseId)
        {
            try
            {
                var games = await gameService.GetGames(EnterpriseId);
                return Ok(games);
            }
            catch (NotFoundItemException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpGet("{Id:int}")]
        public async Task<ActionResult<Game>> getGame(int EnterpriseId, int Id)
        {
            try
            {
                var game = await gameService.GetGame(EnterpriseId, Id);
                return Ok(game);
            }
            catch (NotFoundItemException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }
        [HttpPost]
        public async Task<ActionResult<Game>> PostGame(int EnterpriseId,[FromBody]Game game)
        {
            try
            {
                var gameCreated = await gameService.CreateGame(EnterpriseId, game);
                return Ok(gameCreated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
    }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<Game>> putGame(int EnterpriseId,int id,[FromBody] Game game )
        {
            try
            {
                return Ok(await gameService.UpdateGame(EnterpriseId,id,game));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpPut("{id:int}/updateSales")]
        public async Task<ActionResult<Game>> putGamesSales(int EnterpriseId, int id, [FromBody] Game game)

        {
            try
            {
                return Ok(await gameService.UpdateSales(EnterpriseId, id, game.Sales));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<bool>> DeleteGame(int EnterpriseId, int Id)
        {
            try
            {
                return Ok(await this.gameService.DeleteGame(EnterpriseId,Id));
            }
            catch (NotFoundItemException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {   
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
} 