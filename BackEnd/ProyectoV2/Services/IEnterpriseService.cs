﻿using ProyectoTecWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Services
{
    public interface IEnterpriseService 
    {
       Task< Enterprise> GetEnterpriseAsync(int id,bool showgames);
       Task<Enterprise> CreateEnterpriseAsync(Enterprise enterprise);
       Task<IEnumerable<Enterprise>> GetEnterpisesAsync(bool showgames, string OrderBy);
       Task<bool> DeleteEnterpriseAsync(int id);
       Task<Enterprise> UpdateEnterpriseAsync(int id, Enterprise enterprise);
       
    }
}
