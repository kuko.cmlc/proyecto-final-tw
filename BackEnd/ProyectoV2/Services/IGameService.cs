﻿using ProyectoTecWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoTecWeb.Services
{
    public interface IGameService
    {
       Task<Game> CreateGame(int idEnterprise, Game game);
       Task<Game> GetGame(int idEnterprise, int id);
       Task<IEnumerable<Game>> GetGames(int idEnterprise);
       Task<bool> DeleteGame(int idEnterprise , int id);
       Task<Game>  UpdateGame(int idEnterprise, int id, Game game);
        Task<IEnumerable<Game>> GetGoty();
        Task<Game> UpdateSales(int idEnterprise, int id, int sales);
    }
}
