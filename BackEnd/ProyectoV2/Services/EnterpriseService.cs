﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Data.Repository;
using ProyectoTecWeb.Models;
using ProyectoV2.Exceptions;

namespace ProyectoTecWeb.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        private HashSet<string> allowedOrderByValues;
        private IEnterpriseRepository enterpriseRepository;
        private IMapper mapper;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository , IMapper mapper)
        {
            this.enterpriseRepository = enterpriseRepository;
            this.mapper = mapper;
            allowedOrderByValues = new HashSet<string>() { "name", "country", "city","id"};
        }

        public async Task<Enterprise> CreateEnterpriseAsync(Enterprise enterprise)
        {
            var enterpriseEntity = mapper.Map<EnterpriseEntity>(enterprise);

            enterpriseRepository.CreateEnterprise(enterpriseEntity);
            if (await enterpriseRepository.SaveChangesAsync())
            {
                return mapper.Map<Enterprise>(enterpriseEntity);
            }
            throw new Exception("There were an error with the DB");
        }

        public async Task<bool> DeleteEnterpriseAsync(int id)
        {
            
            await ValidateEnterprise(id);
            await enterpriseRepository.DeleteEnterpriseAsync(id);
            if (await enterpriseRepository.SaveChangesAsync())
            {
                return true;
            }
            return false;

        }

        public async Task<IEnumerable<Enterprise>> GetEnterpisesAsync(bool showgames, string OrderBy)
        {
            var orderByLower = OrderBy.ToLower();
            if (!allowedOrderByValues.Contains(orderByLower))
            {
                throw new BadRequestOperationException($"invalid Order By value : {OrderBy} the only allowed values are {string.Join(", ", allowedOrderByValues)}");
            }
            var enterprisesEntities = await enterpriseRepository.GetEnterpisesAsync(showgames,orderByLower);
            return mapper.Map<IEnumerable<Enterprise>>(enterprisesEntities);
        }

        public async Task<Enterprise> GetEnterpriseAsync(int id, bool showgames)
        {
            var enterpriseEntity = await enterpriseRepository.GetEnterpriseAsync(id, showgames);
            if (enterpriseEntity ==null)
            {
                throw new NotFoundItemException("Enterprise Not Found");
            }
            return mapper.Map<Enterprise>(enterpriseEntity);
        }

        public async Task<Enterprise> UpdateEnterpriseAsync(int id, Enterprise enterprise)
        {
            if(id != enterprise.Id)
            {
                throw new Exception("URL id need to be the same");
            }

            await ValidateEnterprise(id);
            enterprise.Id = id;
            
            var enterpriseEntity = mapper.Map<EnterpriseEntity>(enterprise);
            enterpriseRepository.UpdateEnterprise(enterpriseEntity);
            if (await enterpriseRepository.SaveChangesAsync())
            {
                return mapper.Map<Enterprise>(enterpriseEntity);
            }
            throw new Exception("There ware an error with the DB");
        }
        //Public Just For Tests
        public async Task ValidateEnterprise(int id)
        {
            var enterprise = await enterpriseRepository.GetEnterpriseAsync(id);
            if (enterprise == null)
            {
                throw new NotFoundItemException($"Cannot found enterprise with id {id}");
            }
            enterpriseRepository.DetachEntity(enterprise);
        }
    }
}
