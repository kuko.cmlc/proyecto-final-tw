﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Data.Repository;
using ProyectoTecWeb.Models;
using ProyectoV2.Exceptions;

namespace ProyectoTecWeb.Services
{
    public class GameService : IGameService
    {
        private readonly IEnterpriseRepository enterpriseRepository;
        private readonly IMapper mapper;

        public GameService(IEnterpriseRepository enterpriseRepository, IMapper mapper)
        {
            this.enterpriseRepository = enterpriseRepository;
            this.mapper = mapper;
        }
        public async Task<Game> CreateGame(int idEnterprise, Game game)
        {
          
            var enterpriseEntity = await ValidateEnterpriseId(idEnterprise);
            if (enterpriseEntity!=null)
            {
                game.IdEnterprise= idEnterprise;
                var gameEntity = mapper.Map<GameEntity>(game);
                gameEntity.Enterprise = enterpriseEntity;
                
                enterpriseRepository.CreateGame(gameEntity);
                if (await enterpriseRepository.SaveChangesAsync())
                {
                    return mapper.Map<Game>(gameEntity);
                }
            }         
            throw new BadRequestOperationException("The Enterprise Does not Exist");
        }
          
        public async Task<Game> GetGame(int idEnterprise, int id)
        {
            await ValidateEnterpriseAndGame(idEnterprise,id);
            var gameEntity = await enterpriseRepository.GetGameAsync(id,idEnterprise);
            return mapper.Map<Game>(gameEntity);
        }

        public async Task<IEnumerable<Game>> GetGames(int idEnterprise)
        {
            var gamesEntities = await enterpriseRepository.GetGamesAsync(idEnterprise);
            return mapper.Map<IEnumerable<Game>>(gamesEntities);
        }

        public async Task<Game> UpdateGame(int idEnterprise, int id, Game game)
        {
            if (id != game.Id)
            {
                throw new InvalidOperationException("Error Id game to update is not the samne");
            }
            await ValidateEnterpriseAndGame(idEnterprise,id);
            var enterpriseEntity = await ValidateEnterpriseId(idEnterprise);
            game.Id = id;
            var gameEntity = mapper.Map<GameEntity>(game);
            gameEntity.Enterprise = enterpriseEntity;
            enterpriseRepository.UpdateGame(gameEntity);
            if (await enterpriseRepository.SaveChangesAsync())
            {
                return mapper.Map<Game>(gameEntity);
            }
            throw new Exception("There were an error with the DB");
        }

        private async Task<EnterpriseEntity> ValidateEnterpriseId(int id)
        {
             var enterprise = await enterpriseRepository.GetEnterpriseAsync(id,false);
            if (enterprise == null)
            {
                throw new NotFoundItemException($"Cannot found enterprise with id {id}");
            }
            return enterprise;
        }

        private async Task<bool> ValidateEnterpriseAndGame(int enterpriseId, int gameId)
        {
            var enterprise = await enterpriseRepository.GetEnterpriseAsync(enterpriseId);
            if (enterprise == null)
            {
                throw new NotFoundItemException($"Cannot Found Enterprise with Id {enterpriseId}");
            }
            var game = await enterpriseRepository.GetGameAsync(gameId,enterpriseId);
            if (game == null)
            {
                throw new NotFoundItemException($"Game not found with id {gameId}");
            }
            return true;

        }

        public async Task<bool> DeleteGame(int idEnterprise, int id)
        {
            if (await ValidateEnterpriseAndGame(idEnterprise, id))
            {
                await enterpriseRepository.DeleteGameAsync(id);
                if (await enterpriseRepository.SaveChangesAsync())
                {
                    return true;
                }
                return false;
            }
            return false;
        }


        //NEW 
        public async Task<IEnumerable<Game>> GetGoty()
        {
            var GameEntities = await enterpriseRepository.GetGoty6Async();
            return mapper.Map<IEnumerable<Game>>(GameEntities);
        }



        public async Task<Game> UpdateSales(int idEnterprise, int id, int sales)
        {
            await ValidateEnterpriseAndGame(idEnterprise, id);
            var enterpriseEntity = await ValidateEnterpriseId(idEnterprise);
            var gameEntity = await enterpriseRepository.GetGameAsync(id, idEnterprise);
            gameEntity.Sales = sales;
            gameEntity.Enterprise = enterpriseEntity;
            enterpriseRepository.UpdateGame(gameEntity);
            if (await enterpriseRepository.SaveChangesAsync())
            {
                return mapper.Map<Game>(gameEntity);
            }
            throw new Exception("There were an error with the DB");

        }

    }
}
