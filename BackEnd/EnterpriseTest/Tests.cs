using AutoMapper;
using Moq;
using ProyectoTecWeb.Data;
using ProyectoTecWeb.Data.Entities;
using ProyectoTecWeb.Data.Repository;
using ProyectoTecWeb.Models;
using ProyectoTecWeb.Services;
using ProyectoV2.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EnterpriseTest
{
    public class UnitestWithMoqOnGetAnEnterprise
    {

        [Fact]
        public async void EnterpriseService_ShouldReturnExceptionIfNotFound()
        {
            //Arrange
            int EnterpriseId = 3;
            var MoqEnterpriseRespository = new Mock<IEnterpriseRepository>();
            var enterpriseEntity = new EnterpriseEntity()
            {
                Id = 3,
                Name = "Riot Games Inc.",
                Address = "N/A",
                Postal_Code = 25553,
                Country = "Estados Unidos",
                City = "Los Angeles, California",
                Mail = "ritopls@gmail.com",
                Number_Phone = 45788521,
                Url_Image = "https://www.fullesports.com/wp-content/uploads/2019/12/LOL_CMS_317_Tile_01-Feature_H50-V50-min_bz7waxz83vsjvua6mn4h-e1575392367773.jpg",
                Web_Page = "www.riotgames.com/es",

            };
            MoqEnterpriseRespository.Setup(m => m.GetEnterpriseAsync(EnterpriseId, false)).Returns(Task.FromResult(enterpriseEntity));

            var myProfile = new EnterpriseProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            var enterpriseService = new EnterpriseService(MoqEnterpriseRespository.Object, mapper);
            //act 
            await Assert.ThrowsAsync<NotFoundItemException>(() => enterpriseService.GetEnterpriseAsync(1, false));
        }

        [Fact]
        public async void Validate_Id_Enterprise_ShouldReturnNotFoundItemException()
        {
            //Arrange
            int EnterpriseId = 1;
            var MoqEnterpriseRespository = new Mock<IEnterpriseRepository>();
            var enterpriseEntity = new EnterpriseEntity()
            {
                Id = 1,
                Name = "Riot Games Inc.",
                Address = "N/A",
                Postal_Code = 213,
                Country = "Estados Unidos",
                City = "Los Angeles, California",
                Mail = "ritopls@gmail.com",
                Number_Phone = 45788521,
                Url_Image = "https://www.fullesports.com/wp-content/uploads/2019/12/LOL_CMS_317_Tile_01-Feature_H50-V50-min_bz7waxz83vsjvua6mn4h-e1575392367773.jpg",
                Web_Page = "www.riotgames.com/es",

            };
            MoqEnterpriseRespository.Setup(m => m.GetEnterpriseAsync(EnterpriseId, false)).Returns(Task.FromResult(enterpriseEntity));

            var myProfile = new EnterpriseProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
        
            var enterpriseService = new EnterpriseService(MoqEnterpriseRespository.Object, mapper);
            //act
            await Assert.ThrowsAsync<NotFoundItemException>(() => enterpriseService.ValidateEnterprise(2));
        }
        [Fact]
        public async void Validate_GOTY_ShouldNotReturnNull()
        {
            var ListOfGames = new List<GameEntity>();
            ListOfGames.Add(new GameEntity { Id = 1, Name = "Other",Sales = 43});
            ListOfGames.Add(new GameEntity { Id = 2, Name = "Other", Sales = 52});
            ListOfGames.Add(new GameEntity { Id = 3, Name = "Other", Sales = 93 });
            ListOfGames.Add(new GameEntity { Id = 4, Name = "Other", Sales = 85 });
            ListOfGames.Add(new GameEntity { Id = 5, Name = "Other", Sales = 54 });
            ListOfGames.Add(new GameEntity { Id = 6, Name = "Other", Sales = 688});
            ListOfGames.Add(new GameEntity { Id = 7, Name = "Other", Sales = 32 });
            ListOfGames.Add(new GameEntity { Id = 8, Name = "Other", Sales = 433 });

            IEnumerable<GameEntity> GamesEntities = ListOfGames;
         
            Mock<IEnterpriseRepository> MoqEnterpriseRespository = new Mock<IEnterpriseRepository>();
            MoqEnterpriseRespository.Setup(m => m.GetGoty6Async()).Returns(Task.FromResult(GamesEntities));

            var myProfile = new EnterpriseProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            var gameService = new GameService(MoqEnterpriseRespository.Object, mapper);
            //assert

            var gotylist = await gameService.GetGoty();
            var firtGoty = gotylist.First();
            Assert.Equal(43,firtGoty.Sales);
        }
    }
}  
